
#include "ctype.h"
#include <algorithm> 
#include "htlp.hpp"

static void consumeSpaces (InputStream& input){
    
    char c = input.peek();
    
    while (input.good() && isspace(c)){
        input.get();
        c = input.peek();
    }
    
}

static String parseQuote (InputStream& input){
    
    char delim = input.get();
    String string = "";
    
    for (char c = input.get(); input.good() && c != delim; c = input.get())
        string += c;
    
    return string;
    
}

static String parseString (InputStream& input){
    
    char c = input.peek();
    String string = "";
    
    while (input.good() && isalnum(c)){
        string += c;
        input.get();
        c = input.peek();
    }
    
    return string;
    
}

void Parser::parseOpenTag (InputStream& input){
    
    Tag* root = openTags.back();
    Tag* tag = new Tag;
    
    char c = input.get();
    while (input.good()){
        
        if (c == '>'){
            if (!tag->name.empty()){
                root->childs.push_back (tag);
                openTags.push_back (tag);
            }
            return;
        }
        
        else if (c == '/' && input.peek() == '>'){
            root->childs.push_back (tag);
            return;
        }
        
        else if (isalnum(c)){
            if (tag->name.empty())
                for (; input.good() && isalnum(c); c = input.get())
                    tag->name += c;
            else {
                String name = "";
                String value = "";
                name = ::parseString (input);
                ::consumeSpaces (input);
                c = input.peek ();
                if (c == '='){
                    input.get();
                    ::consumeSpaces (input);
                    c = input.peek ();
                    if (c == '"' || c == '\'' || c == '`')
                        value = ::parseQuote (input);
                    else while (input.good() && isalnum(c)){
                        value += c;
                        c = input.get();
                    }
                }
                tag->attribs[name] = value;
            }
        }
        
        else c = input.get();
        
    }
    
}

void Parser::closeTag (const String& tagName){
    
    auto itr = openTags.end();
    auto begin = openTags.begin();
    while (--itr != begin)
        if ((*itr)->name == tagName) break;
    if (itr == openTags.end()) return;
    openTags.erase (itr, openTags.end());
    
}

void Parser::parseCloseTag (InputStream& input){
    
    char c = input.get ();
    
    String name;
    
    while (input.good()){
        if (isalnum (c)) name += c;
        else if (c == '>'){ closeTag (name); return; }
        c = input.get ();
    }
    
}

Tag Parser::parseHtml (InputStream& input){
    
    Tag root;
    root.name = "ROOT";
    openTags.push_back (&root);
    
    char c = input.get();
    while (input.good()){
        
        if (c == '<'){
            if (input.peek() == '/') parseCloseTag (input);
            else parseOpenTag (input);
        }
        
        else if (isalnum(c)) openTags.back()->text += c;
        
        c = input.get();
        
    }
    
    return root;
    
}
