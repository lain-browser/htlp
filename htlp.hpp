
#ifndef HLTP_H
#define HLTP_H

#include <unordered_map>
#include <iostream>
#include <istream>
#include <string>
#include <vector>
#include <stack>

struct Tag;

typedef std::string String;
typedef std::istream InputStream;

struct Tag {
    
    typedef std::vector <Tag*> TagVector;
    typedef std::unordered_map <String, String> AttributeMap;
    
    int start, end;
    String name;
    String text;
    TagVector childs;
    AttributeMap attribs;
    
    ~Tag ();
    
};

class Parser {
    
    typedef std::vector <Tag*> OpenTagStack;
    OpenTagStack openTags;
    
    void parseOpenTag (InputStream& input);
    void parseCloseTag (InputStream& input);
    void closeTag (const String& tagName);
    
    public:
    
    Tag parseHtml (InputStream& input);
    
};

#endif

